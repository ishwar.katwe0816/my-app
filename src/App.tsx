import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import TimelineSlider from './components/TimelineSlider';
import { getData } from './services/Service';
import VerticalTimeline from './components/VerticalTimeline';
import Desc from './components/Desc';

const StyledContent = styled.div`
   margin:10px;
`;


const TimeLineList = styled.ul`
   padding:0px;
   margin:10px 0px;
   list-style:none;
   display: flex;
   justify-content: space-between;
`;

const TimeLineItems = styled.li`
  padding:0px;
  margin:10px 0px; 
  width:25px;
`;

const SubLabel = styled.label`
  font-size: 12px;
  display: block;
  font-weight: bold;
`;


function App() {
  const [isLoading, setIsLoading] = useState(true);
  const [year, setYear] = useState(0);
  const [data, setData] = useState<any>([]);
  const [limit, setLimit] = useState<any>(0);
  const [selectedYear, setSelectedYear] = useState<any>();
  useEffect(() => {
    setIsLoading(true);
    getData().then(res => {
      setData(res);
      setSelectedYear(res[0]);
      setIsLoading(false);
      setLimit(res.length);
    });
  }, []);

  useEffect(() => {
    data.forEach((item: any, i: number) => {
      const tabYear = year - 1;
      if (i == tabYear) {
        setSelectedYear(item);
      }
    });

    console.log(data, year);
  }, [year]);

  if (isLoading) {
    return <h1>Loading, Please wait...</h1>;
  }
  return (
    <StyledContent>
      Select focus year
      <TimelineSlider val={year} onChange={e => setYear(Number(e.target.value))} min={1} max={limit} step={1} />

      <TimeLineList>
        {data?.map((i: any) => <TimeLineItems key={i.year + i.label}>
          {i.year}
          <SubLabel>{i.label}</SubLabel>
        </TimeLineItems>)}
      </TimeLineList>

      {selectedYear?.months.map((item: any) => <VerticalTimeline title={selectedYear.year} subTitle={item.month}>
        <Desc description={item.description} list={item.list} />
      </VerticalTimeline>)}
    </StyledContent>
  );
}

export default App;
