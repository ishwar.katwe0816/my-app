import styled from 'styled-components';



const StyledContent = styled.div`
   background:#ccc;
   padding:10px;
   display:inline-block;
   min-width:400px;
`;

const Title = styled.h4`
  margin:2px 0px;
`;

const List = styled.ul`
  margin:2px 0px;
  list-style:none;
  padding:0px;
`;

interface IDesc {
    description: string,
    list: Array<string>
}

const Desc = ({ description, list }: IDesc) => {
    return <StyledContent>
        <Title>{description}</Title>

        <List>
            {list.map(i => <li>{i}</li>)}
        </List>
    </StyledContent>
}

export default Desc;