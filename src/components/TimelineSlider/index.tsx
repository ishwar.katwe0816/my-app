import styled from 'styled-components';
import { useEffect, useState } from 'react';

const thumbSize = '30px';
const barHeight = '4px';

const StyledProgressBar = styled.div<any>`
  width: 100%;
  height: ${barHeight};
  cursor: pointer;
  background: grey;
  border-radius: 8x;
  position: relative;
  margin-top: 18px;

  &:after {
    content: ' ';
    position: absolute;
    left: 0;
    top: 0;
    background: blue;
    width: ${(props: any) => props.percentage}%;
    height: inherit;
  }
`;

const StyledContent = styled.div`
    position:relative;
    height:4px;
`;


const StyledInputRange = styled.input`
    appearance: none;
    width: 100%;
    position: absolute;
    z-index: 1;
    &:focus {
    outline: none;
    }
    &::-webkit-slider-runnable-track {
    width: 100%;
    height: 0;
    }
    &::-webkit-slider-thumb {
    height: ${thumbSize};
    width: ${thumbSize};
    border-radius: 50%;
    cursor: pointer;
    appearance: none;
    box-shadow: 1px 1px 0 0 #ccc;
    background: blue;
    transform: translateY(-50%);
`;

export const findPercentage = (min: number, max: number, input: number): number => {
    return ((input - min) * 100) / (max - min);
};

interface ITimelineSlider extends React.InputHTMLAttributes<HTMLInputElement> {
    min?: number;
    max?: number;
    val: number;
}

const TimelineSlider = ({ min = 0, max = 100, val, ...props }: ITimelineSlider) => {

    const [percentage, setPercentage] = useState(findPercentage(+min, +max, +val));

    useEffect(() => {
        setPercentage(findPercentage(+min, +max, +val));
    }, [val]);

    return <StyledContent>
        <StyledInputRange type="range" value={val} min={min} max={max} {...props} />
        <StyledProgressBar percentage={percentage} />
    </StyledContent>
}

export default TimelineSlider;