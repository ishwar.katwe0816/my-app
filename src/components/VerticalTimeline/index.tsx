import styled from 'styled-components';
import { Children } from 'react';

const StyledContent = styled.div`
    border-left :3px dashed #ccc;
    margin: 0px 50px;
    padding: 25px 25px 25px 80px;
`;

const Title = styled.h4`
    color:#000;
    margin:0px;
    padding:0px;
`;

const SubTitle = styled.h3`

    color:#000;
    margin:0px;
    padding:0px;
`;

const TimeLineContent = styled.div`
    position:absolute;
    left: 42px;
    background:#fff;
    padding:2px;
`;

interface IVerticalTimeline {
    title: string;
    subTitle: string;
    children: React.ReactChild;
}

const VerticalTimeline = ({ title, subTitle, children }: IVerticalTimeline) => {
    return <StyledContent>
        <TimeLineContent>
            <Title>{title}</Title>
            <SubTitle>{subTitle}</SubTitle>
        </TimeLineContent>
        {children}
    </StyledContent>
}

export default VerticalTimeline;